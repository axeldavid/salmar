# Required packages
 -timidity++
 -lilypond
 -oggenc
 -lame

### Fedora
```bash
sudo yum install lilypond timidity++ vorbis-tools lame
```
