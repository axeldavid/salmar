/*jslint browser:true */
/*global jQuery, CirclePlayer */

(function ($) {

    "use strict";

    var Plugins = {};

    Plugins.Player = {

        init: function (el) {
            this.CirclePlayer = new CirclePlayer("#jquery_jplayer_1", {
                m4a: el.data('mp3'),
                oga: el.data('ogg')
            }, {
                cssSelectorAncestor: "#cp_container_1"
            });

        }
    };

    Plugins.showLoaderOnSubmit = {

        init: function (el) {

            var loader = el.find('.loading'),
                submit = loader.siblings('input[type=submit]');

            el.on('submit', function () {
                submit.hide();
                loader.show();
            });
        }

    };

    $(document).ready(function () {
        $(document).foundation();
        $('[data-plugin]').each(function () {
            var el = $(this);
            Plugins[el.data('plugin')].init(el);
        });
    });

}(jQuery));
