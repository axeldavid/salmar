from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import RedirectView

from views import login_user, logout_user

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', RedirectView.as_view(url='/hymns/'), name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^hymns/', include('hymns.urls')),
    url(r'^logout/', logout_user, name='logout'),
    url(r"^(?P<page>.*?)login/{0,1}$", login_user, name="login"),
)

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
