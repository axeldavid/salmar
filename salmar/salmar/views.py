from django.contrib.auth.views import logout
from django.contrib.auth.views import login
from django.shortcuts import redirect


def login_user(request, page):

    response = login(request, template_name='login.html')

    if request.user.is_authenticated():
        return redirect('/%s' % page)

    return response


def logout_user(request):
    logout(request)
    return redirect(request.GET.get('next', '/'))
