import os

from django.contrib.auth.models import User
from django.core.files import File as DjangoFile
from django.db import models

from lilypond import Lilypond


class Hymn(models.Model):
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255, blank=True, null=True)
    lyrics = models.TextField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    creator = models.ForeignKey(User)
    published = models.BooleanField(default=True)

    def _get_file(self, ext):
        cache_key = '_CACHE_%s' % ext
        if not hasattr(self, cache_key):
            file_object = File.objects.get(uploaded_file__endswith='.%s' % ext,
                                           hymn=self)
            setattr(self, cache_key, file_object)
        return getattr(self, cache_key)

    def add_file(self, uploaded_file, link_to_hymn=False):

        if isinstance(uploaded_file, str):
            with open(uploaded_file, 'r') as f:
                uploaded_file = DjangoFile(f)

        file_object = File(uploaded_file=uploaded_file, hymn=self,
                           link_to_hymn=link_to_hymn)
        file_object.save()
        return file_object

    def compile_lilypond(self, file_object):
        lilypond_file = Lilypond(file_object.uploaded_file.path)

        for ft in ('midi', 'pdf',):
            file_path = getattr(lilypond_file, 'get_%s' % ft)()
            file_object = self.add_file(file_path, True)
            file_object.save()

        for ft in ('png', 'ogg', 'mp3',):
            file_path = getattr(lilypond_file, 'get_%s' % ft)()
            file_object = self.add_file(file_path, False)
            file_object.save()

    def ly(self):
        return self._get_file('ly')

    def pdf(self):
        return self._get_file('pdf')

    def midi(self):
        return self._get_file('midi')

    def png(self):
        return self._get_file('png')

    def mp3(self):
        return self._get_file('mp3')

    def ogg(self):
        return self._get_file('ogg')


class File(models.Model):
    uploaded_file = models.FileField(upload_to='hymns')
    hymn = models.ForeignKey(Hymn)
    link_to_hymn = models.BooleanField(default=True)

    def url(self):
        return '/' + os.path.relpath(self.uploaded_file.url)
