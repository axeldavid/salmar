import os
import subprocess


class Lilypond(object):

    def __init__(self, path):
        self.path = path
        self.filename, ext = os.path.splitext(path)
        self.root = os.path.dirname(path)

        if ext.lower() != '.ly':
            raise TypeError('Not a lilypond file')

    def _get_path(self, ext, force=False):
        path = '.'.join([self.filename, ext])
        return path if os.path.exists(path) or force else ''

    def call(self, args):
        subprocess.call(args, cwd=self.root)

    def lilypond_compile(self):
        self.call(['lilypond', '--png', '--pdf', self.path])
        png = self._get_path('png')
        os.rename(png, '%s.unedited' % png)

    def get_midi(self):
        path = self._get_path('midi')
        if path:
            return path

        self.lilypond_compile()
        return self.get_midi()

    def get_pdf(self):
        path = self._get_path('pdf')
        if path:
            return path

        self.lilypond_compile()
        return self.get_pdf()

    def get_png(self):
        path = self._get_path('png')
        if path:
            return path

        unedited_path = self._get_path('png.unedited')
        if unedited_path:
            png_path = self._get_path('png', force=True)
            self.call(['convert', unedited_path, '-transparent', 'white', '-fuzz',
                       '25%', '-trim', png_path])
            os.remove(unedited_path)
            return self.get_png()

        self.lilypond_compile()
        return self.get_png()

    def get_wav(self):
        path = self._get_path('wav')
        if path:
            return path

        midi = self.get_midi()
        self.call(['timidity', midi, '-Ow', '-o',
                   self._get_path('wav', force=True)])
        return self.get_wav()

    def get_ogg(self):
        path = self._get_path('ogg')
        if path:
            return path

        wav = self.get_wav()
        self.call(['oggenc', wav])
        return self.get_ogg()

    def get_mp3(self):
        path = self._get_path('mp3')
        if path:
            return path

        wav = self.get_wav()
        self.call(['lame', '-V3', wav, self._get_path('mp3', force=True)])
        return self.get_mp3()
