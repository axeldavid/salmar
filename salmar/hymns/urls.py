from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from hymns.views import HymnFormView, HymnView


urlpatterns = patterns(
    'hymns.views',
    url('^$', HymnView.as_view(), name='hymn'),
    url('^(?P<hymn_id>\d+)/$', HymnView.as_view(), name='view_hymn'),
    url('^add/$', login_required(HymnFormView.as_view()), name='add_hymn'),
    url('^edit/(?P<hymn_id>\d+)/$', login_required(HymnFormView.as_view()), name='edit_hymn'),
)
