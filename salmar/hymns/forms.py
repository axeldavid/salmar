from django import forms
from django.utils.translation import ugettext as _

from models import Hymn


class HymnForm(forms.ModelForm):

    lilypond_file = forms.FileField()

    class Meta:
        model = Hymn
        exclude = ['lyrics', 'creator']
        widgets = {
            'title': forms.TextInput(attrs={
                'placeholder': _('Title'),
                'autofocus': 'autofocus'
            }),
            'author': forms.TextInput(attrs={
                'placeholder': _('Author')
            })
        }
