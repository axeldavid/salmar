from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.http import Http404
from django.views.generic import FormView, TemplateView

from forms import HymnForm
from models import Hymn


class HymnView(TemplateView):

    template_name = 'hymn.html'

    def get(self, request, *args, **kwargs):
        context = {'hymns': Hymn.objects.all().order_by('-created')}
        if 'hymn_id' in self.kwargs:
            try:
                context['hymn'] = Hymn.objects.get(id=self.kwargs['hymn_id'])
            except ObjectDoesNotExist:
                raise Http404
        else:
            try:
                context['hymn'] = Hymn.objects.latest('created')
            except ObjectDoesNotExist:
                context['hymn'] = None

        return self.render_to_response(context)


class HymnFormView(FormView):

    template_name = 'hymn_form.html'
    form_class = HymnForm
    hymn = None

    def get_context_data(self, **kwargs):
        kwargs = super(HymnFormView, self).get_context_data(**kwargs)

        if 'hymn_id' in self.kwargs:
            if not self.hymn:
                try:
                    self.hymn = Hymn.objects.get(id=self.kwargs['hymn_id'])
                except ObjectDoesNotExist:
                    raise Http404
            kwargs.update({'hymn': self.hymn})
        return kwargs

    def get_form_kwargs(self):
        kwargs = super(HymnFormView, self).get_form_kwargs()
        if 'hymn_id' in self.kwargs:
            if not self.hymn:
                try:
                    self.hymn = Hymn.objects.get(id=self.kwargs['hymn_id'])
                except ObjectDoesNotExist:
                    raise Http404
            kwargs.update({'instance': self.hymn})
        return kwargs

    def get_form(self, form_class):
        form = super(HymnFormView, self).get_form(form_class)
        if 'hymn_id' in self.kwargs:
            form.fields['lilypond_file'].required = False
        return form

    def form_valid(self, form):
        form.instance.creator = self.request.user
        hymn = form.save()
        self.hymn_id = hymn.id

        if 'hymn_id' not in self.kwargs:  # If not editing existing
            lilypond_file = self.request.FILES.get('lilypond_file')
            file_object = hymn.add_file(lilypond_file, True)
            hymn.compile_lilypond(file_object)
        return super(HymnFormView, self).form_valid(form)

    def get_success_url(self):
        return reverse('view_hymn', args=(self.hymn_id,))
